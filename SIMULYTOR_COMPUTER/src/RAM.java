public class RAM {

    /**
     * Оперативная память
     */


    private final TypeOfRam typeRam;

    public TypeOfRam getTypeRam() {
        return typeRam;
    }


    private final int volume;

    public int getVolume() {
        return volume;
    }


    public RAM() {
        // все свойства по умолчанию
        this.typeRam = TypeOfRam.STATIC;
        this.volume = 8 * 1024;
    }


    public RAM(TypeOfRam typeRam, int volume) {
        this.typeRam = typeRam;
        this.volume = volume;
    }


    public String TypeOfRamToString(TypeOfRam pTypeRam) {
        return (pTypeRam == TypeOfRam.STATIC) ? "статическая"
                : (pTypeRam == TypeOfRam.DYNAMIC) ? "динамическая"
                : "";
    }


    @Override
    public String toString() {
        String s = this.TypeOfRamToString(this.typeRam);
        return String.format("%d Мб, %s, вес %.2f кг.", this.getVolume(), s, this.getWeightRam());
    }

    public float getWeightRam() {
        return this.typeRam == TypeOfRam.STATIC ? 1.0F
                : this.typeRam == TypeOfRam.DYNAMIC ? 1.5F
                : 0;
    }
}



