public class HDD {

    /**
     * Винчестер
     */


    private final TypeOfHdd typeHdd;

    public TypeOfHdd getTypeHdd() {
        return typeHdd;
    }

    private final int volume;

    public int getVolume() {
        return volume;
    }


    public HDD() {
        // все свойства по умолчанию
        this.typeHdd = TypeOfHdd.SSD;
        this.volume = 320;
    }


    public HDD(TypeOfHdd typeHdd, int volume) {
        this.typeHdd = typeHdd;
        this.volume = volume;
    }


    public String TypeOfHddToString(TypeOfHdd ptypeHdd) {
        return (ptypeHdd == TypeOfHdd.HDD) ? "HDD"
                : (ptypeHdd == TypeOfHdd.SSD) ? "SDD"
                : "";
    }


    @Override
    public String toString() {
        String s = this.TypeOfHddToString(this.typeHdd);
        return String.format("%d Гб, %s, вес %.2f кг.", this.getVolume(), s, this.getWeightHdd());
    }

    public float getWeightHdd() {
        return this.typeHdd == TypeOfHdd.HDD ? 0.8F
                : this.typeHdd == TypeOfHdd.SSD ? 0.3F
                : 0;
    }
}



