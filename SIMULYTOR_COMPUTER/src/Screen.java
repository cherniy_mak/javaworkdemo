public class Screen {

    /**
     * Экран
     */


    private final TypeOfScreen typeScreen;

    public TypeOfScreen getTypeScreen() {
        return typeScreen;
    }


    private final int diag;

    public int getDiag() {
        return diag;
    }


    public final float weight;

    public float getWeight() {
        return weight;
    }


    public Screen() {
        // все свойства по умолчанию
        this.typeScreen = TypeOfScreen.IPS;
        this.diag = 24;
        this.weight = 7F;
    }


    public Screen(TypeOfScreen typeScreen, int diag, float weight) {
        this.typeScreen = typeScreen;
        this.diag = diag;
        this.weight = weight;
    }


    public String TypeOfScreenToString(TypeOfScreen ptypeOfScreen) {
        return (ptypeOfScreen == TypeOfScreen.IPS) ? "IPS"
                : (ptypeOfScreen == TypeOfScreen.TN) ? "TN"
                : (ptypeOfScreen == TypeOfScreen.VA) ? "VA"
                : "";
    }


    @Override
    public String toString() {
        String s = this.TypeOfScreenToString(this.typeScreen);
        return String.format("%d дюйма, %s, вес %.2f кг.", this.getDiag(), s, this.getWeight());
    }
}



