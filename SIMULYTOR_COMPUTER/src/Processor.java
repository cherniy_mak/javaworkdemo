public class Processor {

    /**
     * Процессор
     */


    private final double coreFrequency;

    public double getCoreFrequency() {
        return coreFrequency;
    }


    private final int countOfCore;

    public int getCountOfCore() {
        return countOfCore;
    }


    private final String manufacturer;

    public String getManufacturer() {
        return manufacturer;
    }


    private final float weightProc;

    public float getWeightProc() {
        return weightProc;
    }


    /**
     * Пустой конструктор
     */
    public Processor() {
        // все свойства по умолчанию
        this.coreFrequency = 6;
        this.countOfCore = 11;
        this.manufacturer = "USSR";
        this.weightProc = 0.3F;
    }


    /**
     * Конструктор
     * @param coreFrequency Частота процессора
     * @param countOfCore   Кол-во ядер
     * @param manufacturer  Производитель
     * @param weightProc    Вес
     */
    public Processor(double coreFrequency, int countOfCore, String manufacturer, float weightProc) {
        this.coreFrequency = coreFrequency;
        this.countOfCore = countOfCore;
        this.manufacturer = manufacturer;
        this.weightProc = weightProc;
    }

    @Override
    public String toString() {
        return String.format("'%s': частота %.1f ГГц, ядер %d, вес %.2f кг.",
                manufacturer, coreFrequency, countOfCore, weightProc);
    }
}





