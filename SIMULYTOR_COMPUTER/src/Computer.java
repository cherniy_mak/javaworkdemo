public class Computer {


    private final String vendor;

    public String getVendor() {
        return vendor;
    }


    private final String name;

    public String getName() {
        return name;
    }


    /**
     * Конструктор класса
     *
     * @param vendor
     * @param name
     */

    public Computer(String vendor, String name) {
        this.vendor = vendor;
        this.name = name;
    }

    private Processor proc;

    public Processor getProc() {
        return proc;
    }

    public void setProc(Processor proc) {
        this.proc = proc;
    }


    private RAM ram;

    public RAM getRam() {
        return ram;
    }

    public void setRam(RAM ram) {
        this.ram = ram;
    }


    private HDD disk;

    public HDD getDisk() {
        return disk;
    }

    public void setDisk(HDD disk) {
        this.disk = disk;
    }


    private Screen monitor;

    public Screen getMonitor() {
        return monitor;
    }

    public void setMonitor(Screen monitor) {
        this.monitor = monitor;
    }


    private Keyboard keyboard;

    public Keyboard getKeyboard() {
        return keyboard;
    }

    public void setKeyboard(Keyboard keyboard) {
        this.keyboard = keyboard;
    }


    public float getTotalWeight() {
        float res = 0;
        if (getProc() != null)
            res += getProc().getWeightProc();
        if (getRam() != null)
            res += getRam().getWeightRam();
        if (getDisk() != null)
            res += getDisk().getWeightHdd();
        if (getMonitor() != null)
            res += getMonitor().getWeight();
        if (getKeyboard() != null)
            res += getKeyboard().getWeight();
        return res;
    }


    @Override
    public String toString() {
        System.out.println();
        return String.format("Компьютер '%s' made in %s:\n   " +
                        "1. Процессор: %s\n   " +
                        "2. ОП: %s\n   " +
                        "3. Накопитель: %s\n   " +
                        "4. Монитор: %s\n   " +
                        "5. Клавиатура: %s\n" +
                        "Масса нетто: %.4f кг\n" +
                        "Статус сборки: %s"
                , getName()
                , getVendor()
                , getProc() == null ? "отсутствует" : getProc()
                , getRam() == null ? "отсутствует" : getRam()
                , getDisk() == null ? "отсутствует" : getDisk()
                , getMonitor() == null ? "отсутствует" : getMonitor()
                , getKeyboard() == null ? "отсутствует" : getKeyboard()
                , getTotalWeight()
                , isDone() ? "готов !" : "не готов");
    }


    /**
     * Готов ли компьютер?
     * @return true если готов, false если не готов
     */
    public boolean isDone() {
        return !(getProc() == null || getRam() == null || getDisk() == null || getMonitor() == null || getKeyboard() == null);
    }
}



