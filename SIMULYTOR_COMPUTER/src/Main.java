public class Main {

    /**
     * @param args
     */

    public static void main(String[] args) {

        Computer comp = new Computer("USSR", "Electronika-78");
        comp.setProc(new Processor());
        comp.setRam(new RAM());
        comp.setDisk(new HDD());
        comp.setMonitor(new Screen());
        comp.setKeyboard(new Keyboard());
        //System.out.println(comp);

        // подменить комплектующее, используя параметры и вывести на экран
        comp.setProc(new Processor(30, 8, "ЗАРЯ", 0.25F));
        comp.setKeyboard(new Keyboard(TypeOfKeyboard.WIRELESS, true, 5.5F));
        System.out.println(comp);
    }
}



