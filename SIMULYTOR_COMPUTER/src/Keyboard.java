public class Keyboard {

    /**
     * Клавиутура
     */

    private final TypeOfKeyboard typeKeyboard;

    public TypeOfKeyboard getTypeKeyboard() {
        return typeKeyboard;
    }


    private final boolean isBacklight;

    public boolean isBacklight() {
        return isBacklight;
    }


    private final float weight;

    public float getWeight() {
        return weight;
    }


    /**
     * Конструктор по умолчанию
     */
    public Keyboard() {
        // все свойства по умолчанию
        this.typeKeyboard = TypeOfKeyboard.WIRED;
        this.isBacklight = true;
        this.weight = 0.275F;
    }


    /**
     * Конструктор
     * @param typeKeyboard Тип клавиатуры
     * @param isBacklight Подсветка
     * @param weight Вес
     */
    public Keyboard(TypeOfKeyboard typeKeyboard, boolean isBacklight, float weight) {
        this.typeKeyboard = typeKeyboard;
        this.isBacklight = isBacklight;
        this.weight = weight;
    }


    public String TypeOfKeyboardToString(TypeOfKeyboard ptypeKeyboard) {
        return (ptypeKeyboard == TypeOfKeyboard.WIRED) ? "проводная"
                : (ptypeKeyboard == TypeOfKeyboard.WIRELESS) ? "беспроводная"
                : "";
    }


    @Override
    public String toString() {
        String s = this.TypeOfKeyboardToString(this.typeKeyboard);
        return String.format("%s, %s, вес %.3f кг.", isBacklight ? "с подсветкой" : "без подсветки", s, this.getWeight());
    }
}


