package Dimensions;
public class Program {
    public static void main(String[] args) throws CloneNotSupportedException {
        Dimensions gabar = new Dimensions(1, 2, 3);
        gabar.setLength(5);
        gabar.setWidth(6);
        gabar.setHeight(3);

        BankOfCommodityCargoInformation cargoInfo = new BankOfCommodityCargoInformation(66, "666,Avenue", true, "U96", true, gabar);
        cargoInfo.setWeight(111);
        cargoInfo.setTurnOver(false);
        System.out.println(" Переменная cargoInfo:");
        System.out.println(cargoInfo);

        // обычное присваивание, по ссылке. Т.е. по факту объект у нас создался только один раз
        BankOfCommodityCargoInformation cargo2 = cargoInfo;
        cargo2.getDim().setLength(10);
        System.out.println("\n Переменная cargoInfo (после присвоения setLength(10) переменной cargo2):");
        System.out.println(cargoInfo);

        BankOfCommodityCargoInformation cargo3 = (BankOfCommodityCargoInformation)cargoInfo.clone();
        cargo3.getDim().setLength(100); // объект cargo3 стал независимый от cargoInformation
        System.out.println("\n Переменная cargoInfo (после присвоения setLength(100) переменной cargo3):");
        System.out.println(cargoInfo);

        cargo3.setIsFragile(false);
        System.out.println("\n Переменная cargo3:");
        System.out.println(cargo3);
    }
}

