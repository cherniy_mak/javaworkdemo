package Dimensions;
public class BankOfCommodityCargoInformation implements Cloneable {

    /*
     * Копирование объекта. 
     * Он именно создает новую копию объекта
     */
    @Override
	public Object clone() throws CloneNotSupportedException {
	    BankOfCommodityCargoInformation cloneObj = (BankOfCommodityCargoInformation)super.clone();
        cloneObj.setDim((Dimensions)dim.clone());
        return cloneObj;
	}


    public String toString() {
        return String.format("{ Registration`s number: '%s', Address: '%s', TurnOver: %b, isFragile: %b, Weight: %.2f, Dim: {%s}, Volue: %.2f мм3 }",
                            this.registrationNumber, 
                            this.addressOfDelivery,
                            this.turnOver,
                            this.isFragile,
                            this.weight,
                            this.getDim(), 
                            this.GetVolue()
                    );
    }


    public BankOfCommodityCargoInformation(double weight, String addressOfDelivery, boolean turnOver,
                                           String registrationNumber, boolean isFragile,
                                           Dimensions dim) {
        this.setWeight(weight);
        this.setAddressOfDelivery(addressOfDelivery);
        this.setTurnOver(turnOver);
        this.setRegistrationNumber(registrationNumber);
        this.setIsFragile(isFragile);
        this.setDim(dim);
    }


    /*
     * Вес
     */
    private double weight;

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public double getWeight() {
        return weight;
    }


    /*
     * Адрес доставки
     */
    private String addressOfDelivery;
    
    public void setAddressOfDelivery(String addressOfDelivery) {
        this.addressOfDelivery = addressOfDelivery;
    }

    public String getAddressOfDelivery() {
        return addressOfDelivery;
    }


    /*
     * Можно ли переворачивать?
     */
    private boolean turnOver;

    public void setTurnOver(boolean turnOver) {
        this.turnOver = turnOver;
    }

    public boolean getTurnOver() {
        return turnOver;
    }


    /*
     * Регистр. номер
     */
    private String registrationNumber;

    public void setRegistrationNumber(String registrationNumber) {
        this.registrationNumber = registrationNumber;
    }

    public String getRegistrationNumber() {
        return registrationNumber;
    }


    /*
     * Хрупкий ли груз?
     */
    private boolean isFragile;

    public void setIsFragile(boolean isFragile) {
        this.isFragile = isFragile;
    }

    public boolean getIsFragile() {
        return isFragile;
    }


    /*
     * Габариты
     */
    private Dimensions dim;

    public void setDim(Dimensions dim) {
        this.dim = dim;
    }

    public Dimensions getDim() {
        return dim;
    }


    /*
     * Расчет объема
     */
    public double GetVolue() {
        return this.dim.CalcVolue();
    }
}

