package Dimensions;
/*
 * Класс габариты
 */
public class Dimensions implements Cloneable {

    @Override
	public Object clone() throws CloneNotSupportedException {
	    return super.clone();
	}
    

    public String toString() {
        return "Length: " + length + "мм.; Width: " + width + "мм.; Height: " + height + "мм.;";
    }    


    public Dimensions(double length, double width, double height) {
        this.length = length;
        this.width = width;
        this.height = height;
    }

    /*
     * Длина
     */
    private double length;

    public void setLength(double length) {
        this.length = length;
    }

    public double getLength() {
        return length;
    }


    /*
     * Ширина
     */
    private double width;

    public void setWidth(double width) {
        this.width = width;
    }

    public double getWidth() {
        return width;
    }


    /*
     * Высота
     */
    private double height;
    
    public void setHeight(double height) {
        this.height = height;
    }    

    public double getHeight() {
        return height;
    }


    /*
     * Расчет объема
     */
    public double CalcVolue() {
        return (length * width * height);
    }    
}

