package ElevatorDemo;

public class Elevator {
    /**
     * -3 - minFloor
     * 66 - maxFloor
     * -3 - 66
     */

    private int currentFloor = 1;
    private int minFloor = -3;
    private int maxFloor = 66;
    private int floor = 0;

    public Elevator(int minFloor, int maxFloor) {
        this.minFloor = minFloor;
        this.maxFloor = maxFloor;
    }

    public int getCurrentFloor() {
        return currentFloor;
    }

    public void moveDown(int floor) {
        currentFloor = floor > minFloor ? currentFloor - 1 : currentFloor;
        System.out.println("The current floor: " + currentFloor);
    }

    public void moveUp(int floor) {
        currentFloor = floor < maxFloor ? currentFloor + 1 : currentFloor;
        System.out.println("The current floor: " + currentFloor);
    }

    public void move(int floor) {
        if (floor >= minFloor && floor <= maxFloor && floor != currentFloor) {
            int moveOn = floor > currentFloor ? +1 : -1;
            do {
                currentFloor += moveOn;
                System.out.println("Floor: " + currentFloor);
            } while (floor != currentFloor);
        } else {
            System.out.println("Error");
        }
    }
}
