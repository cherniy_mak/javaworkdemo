package ElevatorDemo;

import java.util.Scanner;

public class Program {
   public static void main(String[] args) {
       Elevator elevator = new Elevator(-3, 66);
       while (true) {
           System.out.println();
           System.out.print("Enter the floor number and press enter: " + "\n");
           int floor = new Scanner(System.in).nextInt();
           elevator.move(floor);
       }
   }
}
