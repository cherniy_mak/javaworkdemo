package PeriodDemo;

public enum PeriodType {
    DAY,
    WEEK,
    MONTH,
    QUARTER,
    YEAR
}