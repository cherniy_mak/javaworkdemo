package PeriodDemo;

public class Program {
    public static void main(String[] args) {

    }

static int PeriodToDay(PeriodType pType) throws Exception {
    int result = (pType == PeriodType.YEAR) ? 365
                : (pType == PeriodType.QUARTER) ? 121
                : (pType == PeriodType.MONTH) ? 31
                : (pType == PeriodType.WEEK) ? 7
                : 1;
    if (pType.ordinal() > PeriodType.WEEK.ordinal()) {
        throw new Exception("Кол-во дней можно определить только по дню либо по неделе");
    }
    return result;
}
}