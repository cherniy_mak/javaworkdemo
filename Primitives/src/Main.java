

public class Main {

    public static void main(String[] args) {
        // TODO: ниже напишите код для выполнения задания:
        //  С помощью цикла и преобразования чисел в символы найдите все коды
        //  букв русского алфавита — заглавных и строчных, в том числе буквы Ё.

        // про StringBuilder https://metanit.com/java/tutorial/7.3.php
        StringBuilder zagB = new StringBuilder();
        StringBuilder proB = new StringBuilder();
        for (char ch = 'Ё'; ch <= 'ё'; ch++) {
            if ((ch > 'Ё' && ch < 'А') || (ch > 'я' && ch < 'ё')) {
                continue;
            }
            //System.out.println(ch + " (" + ((int) ch) + ")");
            if ((ch == 'Ё') || (ch >= 'А' && ch <= 'Я')) {
                zagB.append(ch);
            }
            else {
                proB.append(ch);
            }
        }
        String zagStr = zagB.toString(); // ЁАБВГДЕЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ
        String proStr = proB.toString(); // абвгдежзийклмнопрстуфхцчшщъыьэюяё

        /**
         to-do: посмотреть класс String
                 "мама привет".indexOf("привет") == 5 поиск подстроки
                 "мама привет".indexOf(" ") == 4
                 "мама привет".charAt(4) == " " символ строки по заданному индексу
                 "мама привет".length() == 11 длина строка
                 "мама привет".substring(0, 2) == "ма" копирование от и до такого то символа
                 "мама привет".substring(2) == "ма привет" копирование от и до конца
         */

        // Ё поставим после Е (Ё стоит на первом месте)
        /*int pos0 = zagStr.indexOf('Ё'), pos1 = zagStr.indexOf('Е');
        if (pos0 == 0 && pos1 > pos0) {
            zagStr = zagStr.substring(1, pos1+1) + // АБВГДЕ
                    zagStr.charAt(pos0) + //Ё
                    zagStr.substring(pos1 + 1); // ЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ
        } //*/
        zagStr = InsertAfterChar(zagStr, 'Ё', 'Е');
        System.out.println("Zag Str = " + zagStr);

        // ё поставим после е (ё стоит на последнем месте)
        /*int posL = proStr.indexOf('ё'), pose = proStr.indexOf('е');
        if (posL == proStr.length() - 1 && pose < posL) {
            proStr = proStr.substring(0, pose + 1) + // абвгде
                    proStr.charAt(posL) + //ё
                    proStr.substring(pose + 1, proStr.length() - 1);
        } //*/
        proStr = InsertAfterChar(proStr, 'ё', 'е');
        System.out.println("Pro Str = " + proStr);

        System.out.println();
        if (zagStr.length() > 0 && zagStr.length() == proStr.length()) {
            for (int i = 0; i < zagStr.length(); i++) {
                char z = zagStr.charAt(i);
                char p = proStr.charAt(i);
                //System.out.println(z + " (" + (int)z + "), " + p + " (" + (int)p + ")");
                System.out.printf("%s (%d), %s (%d)\n", z, (int)z, p, (int)p);
            }
        }
        else {
            System.out.println("Что-то пошло не так");
        }
    }


    /**
     * Переносит символ A после символа B в строке
     * @param Str Исходная строка
     * @param A
     * @param B
     * @return
     */
    public static String InsertAfterChar(String Str, char A, char B) {
        int pA = Str.indexOf(A);
        int pB = Str.indexOf(B);
        if (pA == -1 || pB == -1 || pA == pB)
            return Str;
        StringBuilder res = new StringBuilder();
        for (int i = 0; i < Str.length(); i++) {
            if (i == pA)
                continue;
            res.append(Str.charAt(i));
            if (i == pB)
                res.append(Str.charAt(pA));
        }
        return res.toString();
    }
}













