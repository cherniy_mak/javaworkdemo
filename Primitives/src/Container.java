public class Container {

    private Integer count = Integer.valueOf(1); // or private int count; по умолчанию = 0


    public void addCount(int value) {
        count = count + value;
    }

    public int getCount() {
        return count;
    }
}

